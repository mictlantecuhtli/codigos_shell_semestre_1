#!/bin/bash
# Fecha: 17-08-2022
# Autor: Fatima Azucena MC
# Correo: fatimaazucenamartinez274@gmail.com

echo "Digite un número: "
read n

a=1

while [ $n -ge $a ]
do
        suma=$((suma+a))
        let a=$a+1
done

echo "El resultado de la suma es: " $suma

