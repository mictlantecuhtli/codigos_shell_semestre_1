
# Fecha: 25-01-2022
# Autor: Fatima Azucena MC
# Correo: fatimaazucenamartinez274@gmail.com

sumatoria=0

for (( i = 1; i <= 3; i++ )) do
	echo "Digite la edad del alumno " $i
	read edad
	sumatoria=$(($sumatoria+$edad))
done
promedio=$(echo "scale=4;$sumatoria/3" | bc -l)
echo "El promedio de edades es: " $promedio
