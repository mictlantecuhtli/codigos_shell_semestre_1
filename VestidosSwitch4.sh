#!/bin/bash
# Fecha: 25-01-2022
# Autor: Fatima Azucena MC
# Correo: fatimaazucenamartinez274@gmail.com

total=0

echo "Selecciona la talla"
echo "1.-Talla chica"
echo "2.-Talla mediana"
echo "3.-Talla grande"
echo "Digite su opción: "
read opcion

case $opcion in
	1)
		echo "Seleccionaste la talla chica (Talla 7)"
		echo "¿Cuántos vestidos compraste?: "
		read cantidad
		total=$(echo "scale=4;$cantidad*300" | bc -l)
	;;
	2)
		echo "Seleccionaste la talla mediana (Talla 10)"
                echo "¿Cuántos vestidos compraste?: "
                read cantidad
                total=$(echo "scale=4;$cantidad*400" | bc -l)
	;;
	3)
		echo "Seleccionaste la talla grande (Talla 16)"
                echo "¿Cuántos vestidos compraste?: "
                read cantidad
                total=$(echo "scale=4;$cantidad*500" | bc -l)
	;;
esac
echo "El total a pagar es: " $total " pesos"
