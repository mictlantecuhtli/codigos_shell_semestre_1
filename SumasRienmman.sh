# Fecha: 19-07-2023
# Autor: Fatima Azucena MC
# Correo: fatimaazucenamartinez274@gmail.com

function funcionS() {
   local x=$1
   echo "$((x*x))"
}

a=0
b=1
n=100
suma=0

w=$(bc <<< "scale=10; ($b - $a) / $n" | bc -l)

for (( i=0; i< $n; i++ )) do
    x=$(bc <<< "scale=10; $a + $i * $w" | bc -l)
    h=$(funcionS $x)
    suma=$(bc <<< "scale=10; $riemann_sum + $h * $w" | bc -l)
done

echo "El resultado de la suma de Riemann es: " $suma
