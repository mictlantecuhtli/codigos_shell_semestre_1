#!/bin/bash
# Fecha: 25-01-2022
# Autor: Fatima Azucena MC
# Correo: fatimaazucenamartinez274@gmail.com

x=0
y=1
z=1

echo "Ingrese el número para la serie de Fibonacci:"
read numero

for (( i = 1; i<= $numero; i++ )) do
	c=1
	for (( a = 1; a < ( $numero - $i + 1 ); a++ )) do
		echo  " "
	done
	for (( b = 1; b <= $i; b++ )) do
		echo -n $c ""
		c=$(echo "scale=0;$c * ( $i - $b) / $b" | bc -l)
	done
	echo " "
done

echo ""

