#!/bin/bash
# Fecha: 17-08-2022
# Autor: Fatima Azucena MC
# Correo: fatimaazucenamartinez274@gmail.com

echo "Ingrese un valor: "
read a
echo "Ingrese otro valor: "
read b

resultado=$(echo "scale=4;$a+$b" | bc -l)

echo "El resultado de la suma es: " $resultado

