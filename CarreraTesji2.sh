#!/bin/bash
# Fecha: 17-08-2022
# Autor: Fatima Azucena MC
# Correo: fatimaazucenamartinez274@gmail.com

cred=0

echo "¿Participaste en el desfíle?"
echo "1.-Si"
echo "0.-No"
echo "Digite su opción: "
read opcion

if [ $opcion -eq 1 ]; then
	cred=$(echo "scale=4;$cred+1" | bc -l)
	echo "Ganaste un crédito, ¿En qué taller estás inscrito?: "
	echo "1.-Danza"
	echo "2.-Basquetbol"
	echo "3.-Futbol"
	echo "4.-TKD"
	echo "5.-Voleibol"
	echo "Digite su opción: "
	read taller

	if [ $taller -eq 1 ]; then
		echo "Uniforme escolar"
	elif [ $taller -eq 2 ]; then
		echo "Blanco"
	elif [ $taller -eq 3 ]; then
                echo "Blanco"
	elif [ $taller -eq 4 ]; then
                echo "Blanco"
	elif [ $taller -eq 5 ]; then
                echo "Blanco"
	fi
else
	echo "No tienes ningun crédito"
fi

echo "¿Participaste en la carrera?"
echo "1.-Si"
echo "0.-No"
echo "Digite su opción: "
read carrera

if [ $carrera -eq 1 ]; then
	cred=$(echo "scale=4;$cred+1" | bc -l)
	
	echo "¿Quedaste en uno de los primeros tres lugares?"
	echo "1.-Si"
	echo "0.-No"
	echo "Digite su respuesta: "
	read lugar

	if [ $lugar -eq 1 ]; then
		cred=$(echo "scale=4;$cred+1" | bc -l)
	fi
fi

echo "El total de créditos obtenidos es: " $cred
