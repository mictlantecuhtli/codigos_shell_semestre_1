#!/bin/bash
# Fecha: 17-08-2022
# Autor: Fatima Azucena MC
# Correo: fatimaazucenamartinez274@gmail.com

echo "Digite hasta que número desea ver el factorial: "
read n

a=1
fact=1

while [ $n -ge $a ]
do
        b=$((a*fact))
        echo $fact " x " $a " = " $b
        fact=$((fact*a))
        let a=$a+1

done
