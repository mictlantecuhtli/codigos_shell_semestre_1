#!/bin/bash
# Fecha: 17-08-2022
# Autor: Fatima Azucena MC
# Correo: fatimaazucenamartinez274@gmail.com

e=4
result=()

echo "Arreglo 1"
for (( i = 0; i < 5; i++ )) do
        echo "Ingrese el número de la posición " $i ": "
        read a
        a1[$i]=$a
done
echo " "
for (( y = 0; y < 5; y++ )) do
        echo ${a1[$y]}
done
echo " "
echo "Arreglo 2"
for (( z = 0; z < 5; z++ )) do
        echo "Ingrese el número de la posición " $z ": "
        read b
        b1[$z]=$b
done
echo " "
for (( c = 0; c < 5; c++ )) do
        echo ${b1[$c]}
done
echo " "
echo "Resultado"
for (( d = 0; d < 5; d++ )) do
	result[$d]=$((${a1[$d]}*${b1[$e]}))
	e=$((e-1))
	echo ${result[$d]}
done


