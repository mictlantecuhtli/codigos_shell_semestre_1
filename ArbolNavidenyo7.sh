#!/bin/bash
# Fecha: 25-01-2023
# Autor: Fatima Azucena MC
# Correo: fatimaazucenamartinez274@gmail.com

numero=8
Num_ESP=`expr $numero - 1`
Num_Ast=1
while [ $numero != 0 ]
do
        for j in `seq 1 $Num_ESP`
        do
                echo -n " "
        done


        for n in `seq 1 $Num_Ast`
        do
                echo -n "*"
        done


        for j in `seq 1 $Num_ESP`
        do
                echo -n " "
        done


echo ""
numero=`expr $numero - 1`
Num_ESP=`expr $Num_ESP - 1`
Num_Ast=`expr $Num_Ast + 2`

done
for (( j = 1; j <= 4; j++ )) do 
	echo "      * * "
done
