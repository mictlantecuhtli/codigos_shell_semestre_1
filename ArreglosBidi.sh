#!/bin/bash
# Fecha: 17-08-2022
# Autor: Fatima Azucena MC
# Correo: fatimaazucenamartinez274@gmail.com

materias=("Cálculo Diferencial" "Fundamentos de programación" "Química" "Fundamentos de investigación" "Matemáticas Discretas" "Desarrollo Sustentable")

declare -a matrix
matrix=()

for (( i = 0; i < 6; i++ )) do
	for (( y = 0; y < 5; y++ )) do
		echo "Ingrese la calificación de la Unidad " $y " de " ${materias[$i]} ": "
		read cal
		matrix[$i][$y] = $cal
	done
done
