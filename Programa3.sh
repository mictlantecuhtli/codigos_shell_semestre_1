#!/bin/bash
# Fecha: 17-08-2022
# Autor: Fatima Azucena MC
# Correo: fatimaazucenamartinez274@gmail.com

echo "Digite la tabla que desea ver: "
read tabla
echo "¿Hasta dónde desea visualizar?: "
read tope

a=1

while [ $tope -ge $a ]
do

	resultado=$((tabla*a))
	echo $tabla " x " $a " = " $resultado
	let a=$a+1
done

