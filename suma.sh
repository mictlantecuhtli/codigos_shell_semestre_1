#!/bin/bash

# Función que representa la función que quieres integrar
# Puedes cambiar esta función según tus necesidades
function mi_funcion() {
    local x=$1
    # Aquí puedes definir la función que deseas integrar
    # Por ejemplo, la siguiente línea representa la función f(x) = x^2
    echo "$((x * x))"
}

# Definir los límites del intervalo de integración
a=0       # Límite inferior
b=1       # Límite superior
n=100     # Número de subintervalos (mayor n implica mayor precisión)

# Calcular el ancho de cada subintervalo
ancho=$(bc <<< "scale=10; ($b - $a) / $n")

# Inicializar la variable para almacenar la suma de Riemann
riemann_sum=0

# Calcular la suma de Riemann
for ((i=0; i<$n; i++)); do
    x=$(bc <<< "scale=10; $a + $i * $ancho")
    fx=$(mi_funcion $x)
    riemann_sum=$(bc <<< "scale=10; $riemann_sum + $fx * $ancho")
done

echo "La suma de Riemann es: $riemann_sum"

