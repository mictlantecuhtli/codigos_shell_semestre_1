#!/bin/bash
# Fecha: 17-08-2022
# Autor: Fatima Azucena MC
# Correo: fatimaazucenamartinez274@gmail.com

titulos=("Unidad 1","Unidad 2","Unidad 3","Unidad 4","Unidad 5")
calificaciones=()

for (( i = 1; i <= 5; i++ )) do
	echo "Ingrese la calificación de la Unidad  "$i ": "
	read calificacion
	calificaciones[$i]=$calificacion
	sumatoria=$(($sumatoria+${calificaciones[$i]}))
done
promedio=$(($sumatoria/5))
echo  "Unidad 1     Unidad 2     Unidad 3     Unidad 4     Unidad 5"

for (( y = 1; y <= 5; y++)) do
	echo -n "    "${calificaciones[$y]}"      "
done
echo " "
echo "El promedio es de: " $promedio
