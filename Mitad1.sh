#!/bin/bash
# Fecha: 17-08-2022
# Autor: Fatima Azucena MC
# Correo: fatimaazucenamartinez274@gmail.com

echo "Digite un número (0-200): "
read numero

doble=$(echo "scale=4;$numero*2" | bc -l)
tercera=$(echo "scale=4;$doble/3" | bc -l)
mitad=$(echo "scale=4;$tercera/2" | bc -l)

echo "La mitad de la tercera parte de " $numero " es: " $mitad
