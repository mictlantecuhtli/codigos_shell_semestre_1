#!/bin/bash
# Fecha: 17-08-2022
# Autor: Fatima Azucena MC
# Correo: fatimaazucenamartinez274@gmail.com

totalPagar=0
descuento=0
totalPagarDescuento=0

echo "¿De qué color es la pelota que haz sacado?"
echo "1.-Verde"
echo "2.-Amarilla"
echo "3.-Roja"
echo "Digite su respuesta: "
read color

if [ $color -eq 1 ]; then
	echo "Felicidades, tiene un descuento del 10%"
	echo "¿Cúal es tu monto a pagar?: "
	read totalPagar
	descuento=$(echo "scale=4;$totalPagar*0.10" | bc -l)
	totalPagarDescuento=$(echo "scale=4;$totalPagar-$descuento" | bc -l)	
elif [ $color -eq 2 ]; then
	echo "Felicidades, tiene un descuento del 5%"
        echo "¿Cúal es tu monto a pagar?: "
        read totalPagar
        descuento=$(echo "scale=4;$totalPagar*0.05" | bc -l)
        totalPagarDescuento=$(echo "scale=4;$totalPagar-$descuento" | bc -l)
elif [ $color -eq 3 ]; then
	echo "Felicidades, tiene un descuento del 15%"
        echo "¿Cúal es tu monto a pagar?: "
        read totalPagar
        descuento=$(echo "scale=4;$totalPagar*0.15" | bc -l)
	totalPagarDescuento=$(echo "scale=4;$totalPagar-$descuento" | bc -l)
else
	echo "Número inválido"
fi

echo "El total a pagar es: " $totalPagarDescuento
